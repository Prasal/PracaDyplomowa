package pl.wat.biblegraph.web.domain.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.wat.biblegraph.common.model.ResponseArray;
import pl.wat.biblegraph.web.api.GraphCriteria;
import pl.wat.biblegraph.web.domain.service.JungService;


@RestController
public class Controller {

    @Autowired
    private JungService jungService;

    @RequestMapping(value = "/getGraph", method = RequestMethod.GET)
    public ResponseArray getGraph(@ModelAttribute GraphCriteria graphCriteria) {
        return jungService.fetchGraphData(graphCriteria);
    }
}
