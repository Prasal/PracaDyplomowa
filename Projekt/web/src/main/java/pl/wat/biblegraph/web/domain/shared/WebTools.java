package pl.wat.biblegraph.web.domain.shared;

import java.awt.Dimension;

import edu.uci.ics.jung.algorithms.layout.AbstractLayout;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.KKLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.SpringLayout;
import edu.uci.ics.jung.graph.Graph;
import pl.wat.biblegraph.common.enums.LayoutEnum;
import pl.wat.biblegraph.common.model.PersonRecord;

/**
 * Created by Lenovo on 2017-07-11.
 */

public class WebTools {
    public static Layout<PersonRecord, String> getLayout(Graph<PersonRecord, String> g, Dimension size, LayoutEnum layoutEnum){
        AbstractLayout<PersonRecord, String> layout;

        if(layoutEnum == LayoutEnum.FRlayout)
            layout = new FRLayout<>(g);
        else if(layoutEnum == LayoutEnum.ISOMlayout)
            layout = new ISOMLayout<>(g);
        else if(layoutEnum == LayoutEnum.CircleLayout)
            layout = new CircleLayout<>(g);
        else if(layoutEnum == LayoutEnum.KKlayout)
            layout = new KKLayout<>(g);
        else if(layoutEnum == LayoutEnum.SpringLayout)
            layout = new SpringLayout<>(g);
        else
            return null;  //Exception

        layout.setSize(size);
        return layout;
    }
}
