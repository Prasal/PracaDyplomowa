package pl.wat.biblegraph.web.api;

import lombok.Data;
import pl.wat.biblegraph.common.enums.GroupEnum;
import pl.wat.biblegraph.common.enums.LayoutEnum;

/**
 * Created by Lenovo on 2017-08-11.
 */

@Data
public class GraphCriteria {
    private int width;
    private int height;
    private GroupEnum group;
    private LayoutEnum layout;
}
