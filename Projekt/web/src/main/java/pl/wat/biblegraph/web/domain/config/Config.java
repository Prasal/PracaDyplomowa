package pl.wat.biblegraph.web.domain.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import pl.wat.biblegraph.common.shared.Measure;

/**
 * Created by Lenovo on 2017-11-28.
 */

@Configuration
public class Config {
    @Bean
    public Measure measure(){
        return new Measure();
    }
}
