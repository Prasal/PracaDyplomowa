package pl.wat.biblegraph.web.domain.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Lenovo on 2017-11-28.
 */

public class InputStreamReaderService {
    private static final String PATH = "web/src/main/resources/";

    public static InputStreamReader getInputStreamReader(String filename){
        File file = new File(PATH + filename);

        if(!file.exists()){
            ClassLoader classLoader = InputStreamReaderService.class.getClassLoader();
            file = new File( classLoader.getResource(filename).getFile() );
        }

        InputStream targetStream = null;

        try {
            targetStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return new InputStreamReader(targetStream);
    }
}
