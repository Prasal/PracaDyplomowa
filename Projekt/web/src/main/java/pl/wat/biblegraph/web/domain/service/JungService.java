package pl.wat.biblegraph.web.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import javax.swing.JFrame;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import pl.wat.biblegraph.common.shared.LoaderImpl;
import pl.wat.biblegraph.common.enums.GroupEnum;
import pl.wat.biblegraph.common.model.MaxCentralityDto;
import pl.wat.biblegraph.common.model.ResponseArray;
import pl.wat.biblegraph.common.model.ResponseEdgeDto;
import pl.wat.biblegraph.common.model.ResponseGraphInfoDto;
import pl.wat.biblegraph.common.model.ResponseVertexDto;
import pl.wat.biblegraph.common.shared.CommonData;
import pl.wat.biblegraph.common.shared.CommonTools;
import pl.wat.biblegraph.common.model.PersonRecord;
import pl.wat.biblegraph.common.model.Point;
import pl.wat.biblegraph.common.model.RelationKey;
import pl.wat.biblegraph.web.api.GraphCriteria;
import pl.wat.biblegraph.common.shared.Measure;
import pl.wat.biblegraph.web.domain.shared.WebTools;

@Service
public class JungService {

    @Autowired
    private Measure measure;

    private static int edgeIterator = 0;

    public ResponseArray fetchGraphData(GraphCriteria graphCriteria) {
        CommonData data = new CommonData();

        LoaderImpl loader = new LoaderImpl(data);
        loader.init(
                InputStreamReaderService.getInputStreamReader("People.csv"),
                InputStreamReaderService.getInputStreamReader("PeopleRelationships.csv"),
                InputStreamReaderService.getInputStreamReader("PeopleGroups.csv")
        );

        Graph<PersonRecord, String> g = createGraph(data, graphCriteria.getGroup());
        Dimension size = new Dimension(graphCriteria.getWidth(), graphCriteria.getHeight());
        Layout<PersonRecord, String> layout = WebTools.getLayout(g, size, graphCriteria.getLayout());
        VisualizationViewer<PersonRecord, String> vv = new VisualizationViewer<>(layout);

        JFrame f = new JFrame();
        f.getContentPane().add(vv);
        f.setSize(size);
        f.setLocationRelativeTo(null);

        vv.repaint();

        return prepareResponse(g, layout);
    }

    private ResponseArray prepareResponse(Graph<PersonRecord, String> g, Layout<PersonRecord, String> layout){
        List<ResponseVertexDto> responseVertexDtos = new ArrayList<>();
        List<ResponseEdgeDto> responseEdgeDtos = new ArrayList<>();

        Map<String, Number> variousMeasuresForGraphMap = measure.measureDiameter(g);
        Map<Integer, Map<String, Number>> centralityForVerticesMap = measure.measureCentrality(g);
        Map<String, MaxCentralityDto> maxCentralitiesForGraphMap = measure.measureMax(centralityForVerticesMap);

        for (PersonRecord personRecord : g.getVertices()) {
            Point2D point2D = layout.transform(personRecord);
            Point point = new Point(point2D.getX(), point2D.getY());

            ResponseVertexDto responseVertexDto = new ResponseVertexDto();
            responseVertexDto.setPersonRecord(personRecord);
            responseVertexDto.setPoint(point);
            responseVertexDto.setMeasuresMap( centralityForVerticesMap.get(personRecord.getId()) );
            responseVertexDtos.add(responseVertexDto);
        }

        for (String s : g.getEdges()) {
            Pair<PersonRecord> endpoints = g.getEndpoints(s);

            ResponseEdgeDto responseEdgeDto = new ResponseEdgeDto();
            responseEdgeDto.setPrimary(endpoints.getFirst());
            responseEdgeDto.setRelatedTo(endpoints.getSecond());
            responseEdgeDto.setRelType(CommonTools.getRelTypeByValue(s.substring(0, s.indexOf("_"))));

            responseEdgeDtos.add(responseEdgeDto);
        }

        ResponseGraphInfoDto responseGraphInfoDto = new ResponseGraphInfoDto();
        responseGraphInfoDto.setVariousMeasuresForGraph(variousMeasuresForGraphMap);
        responseGraphInfoDto.setMaxCentralitiesForGraphMap(maxCentralitiesForGraphMap);

        ResponseArray responseArray = new ResponseArray();
        responseArray.setResponseVertexDtos(responseVertexDtos);
        responseArray.setResponseEdgeDtos(responseEdgeDtos);
        responseArray.setResponseGraphInfoDto(responseGraphInfoDto);

        return responseArray;
    }

    private Graph<PersonRecord, String> createGraph(CommonData data, GroupEnum group){
        Graph<PersonRecord, String> g = new UndirectedSparseGraph<>();

        Set<PersonRecord> personsFromGroup = data.getPersonsFromGroup(group.getGroupName());
        Set<RelationKey> relationSet = data.getRelationSet();

        Function<RelationKey, PersonRecord> getPrimary = relation -> data.getPersonRecord(relation.getPrimary());
        Function<RelationKey, PersonRecord> getRelatedTo = relation -> data.getPersonRecord(relation.getRelatedTo());

        relationSet.stream()
                .filter(i -> personsFromGroup.contains(getPrimary.apply(i)) && personsFromGroup.contains(getRelatedTo.apply(i)))
                .forEach(relation -> g.addEdge(relation.getRelTypeByValue().getValue() + "_" + edgeIterator++, getPrimary.apply(relation), getRelatedTo.apply(relation)));
        
        return g;
    }
}