package pl.wat.biblegraph.app.ui.common;

import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.widget.Toast;

import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;

/**
 * Created by Lenovo on 2017-11-06.
 */

public class BibleGraphToast {
    private static final float FLOAT_TEXT_SIZE_FACTOR = 1.35f;

    public static Toast prepareToast(String text, int duration){
        SpannableStringBuilder biggerText = new SpannableStringBuilder(text);
        biggerText.setSpan(new RelativeSizeSpan(FLOAT_TEXT_SIZE_FACTOR), 0, text.length(), 0);

        return Toast.makeText(
                BibleGraphApp.getContext(),
                biggerText,
                duration);
    }
}
