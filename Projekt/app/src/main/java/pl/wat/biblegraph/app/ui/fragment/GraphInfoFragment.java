package pl.wat.biblegraph.app.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.wat.biblegraph.R;
import pl.wat.biblegraph.common.model.PersonRecord;
import pl.wat.biblegraph.app.domain.shared.ApiTools;
import pl.wat.biblegraph.app.domain.shared.Data;
import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;


public class GraphInfoFragment extends Fragment {

    @Inject
    Data data;

    @Inject
    ApiTools apiTools;

    private final String TAG = this.getClass().getSimpleName();
    private View v;

    protected @BindView(R.id.numberOfVerticesTextView) TextView numberOfVerticesTextView;
    protected @BindView(R.id.numberOfEdgesTextView) TextView numberOfEdgesTextView;

    protected @BindView(R.id.maxEigenOwnerTextView) TextView maxEigenOwnerTextView;
    protected @BindView(R.id.maxClosenessOwnerTextView) TextView maxClosenessOwnerTextView;
    protected @BindView(R.id.maxBetweennessOwnerTextView) TextView maxBetweennessOwnerTextView;
    protected @BindView(R.id.maxDegreeOwnerTextView) TextView maxDegreeOwnerTextView;

    protected @BindView(R.id.maxEigenTextView) TextView maxEigenTextView;
    protected @BindView(R.id.maxClosenessTextView) TextView maxClosenessTextView;
    protected @BindView(R.id.maxBetweennessTextView) TextView maxBetweennessTextView;
    protected @BindView(R.id.maxDegreeTextView) TextView maxDegreeTextView;

    protected @BindView(R.id.diameterTextView) TextView diameterTextView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.drawer_graph_info, container, false);
            ButterKnife.bind(this, v);
        }

        ((BibleGraphApp) getActivity().getApplication()).getComponent().inject(this);

        updateDataOnDrawer();

        return v;
    }

    private void updateDataOnDrawer(){
        numberOfVerticesTextView.setText(String.valueOf(data.getPositionMap().size()));
        numberOfEdgesTextView.setText(String.valueOf(data.getRelationSet().size()));

        maxEigenOwnerTextView.setText(getNameAndSurname(data.getMaxCentraliyForGraph("maxEigen").getId()));
        maxClosenessOwnerTextView.setText(getNameAndSurname(data.getMaxCentraliyForGraph("maxCloseness").getId()));
        maxBetweennessOwnerTextView.setText(getNameAndSurname(data.getMaxCentraliyForGraph("maxBetweenness").getId()));
        maxDegreeOwnerTextView.setText(getNameAndSurname(data.getMaxCentraliyForGraph("maxDegree").getId()));

        maxDegreeTextView.setText(apiTools.measureToString(data.getMaxCentraliyForGraph("maxDegree").getValue()));
        maxClosenessTextView.setText(apiTools.measureToString(data.getMaxCentraliyForGraph("maxCloseness").getValue()));
        maxBetweennessTextView.setText(apiTools.measureToString(data.getMaxCentraliyForGraph("maxBetweenness").getValue()));
        maxEigenTextView.setText(apiTools.measureToString(data.getMaxCentraliyForGraph("maxEigen").getValue()));

        diameterTextView.setText(apiTools.measureToString(data.getMeasureForGraph("diameter")));
    }

    private String getNameAndSurname(int id){
        PersonRecord personRecord = data.getPersonRecord(id);
        return personRecord.getSurname().equals("-") ? personRecord.getName() : personRecord.getName() + " " +  personRecord.getSurname();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
    }
}
