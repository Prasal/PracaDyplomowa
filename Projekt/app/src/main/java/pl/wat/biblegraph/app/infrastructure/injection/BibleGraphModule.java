package pl.wat.biblegraph.app.infrastructure.injection;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.wat.biblegraph.app.domain.shared.ApiTools;
import pl.wat.biblegraph.app.domain.shared.Data;
import pl.wat.biblegraph.app.infrastructure.repository.RepositoryService;
import pl.wat.biblegraph.app.infrastructure.repository.UriContainer;
import pl.wat.biblegraph.app.domain.service.SharedPreferencesService;
import pl.wat.biblegraph.common.shared.Measure;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class BibleGraphModule {

    @Provides
    @Singleton
    Data provideData(){
        return new Data();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson) {
        return new Retrofit.Builder()
                .baseUrl("http://www.example.com")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    public RepositoryService provideRepositoryService(Retrofit retrofit) {
        return retrofit.create(RepositoryService.class);
    }

    @Provides
    @Singleton
    public SharedPreferencesService sharedPreferencesService() {
        return new SharedPreferencesService();
    }

    @Provides
    @Singleton
    public ApiTools apiTools() {
        return new ApiTools();
    }

    @Provides
    @Singleton
    Measure provideMeasure() {
        return new Measure();
    }
}
