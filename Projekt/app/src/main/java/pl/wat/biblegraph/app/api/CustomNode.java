package pl.wat.biblegraph.app.api;

import android.graphics.Shader;

import net.xqhs.graphs.graph.SimpleNode;

import pl.wat.biblegraph.common.model.PersonRecord;
import lombok.Getter;

/**
 * Created by Lenovo on 2017-07-22.
 */

@Getter
public class CustomNode extends SimpleNode {
    private PersonRecord personRecord;
    private Shader shader;

    public CustomNode(PersonRecord personRecord, Shader shader) {
        super(personRecord.getName());
        this.personRecord = personRecord;
        this.shader = shader;
    }
}
