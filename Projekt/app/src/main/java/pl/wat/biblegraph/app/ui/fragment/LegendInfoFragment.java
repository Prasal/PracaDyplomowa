package pl.wat.biblegraph.app.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.wat.biblegraph.R;
import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;


public class LegendInfoFragment extends Fragment {

    private final String TAG = this.getClass().getSimpleName();
    private View v;

    protected @BindView(R.id.degreeOverallDescTextView) TextView degreeOverallDescTextView;
    protected @BindView(R.id.degreeTextView) TextView degreeTextView;
    protected @BindView(R.id.closenessTextView) TextView closenessTextView;
    protected @BindView(R.id.betweennessTextView) TextView betweennessTextView;
    protected @BindView(R.id.eigenTextView) TextView eigenTextView;
    protected @BindView(R.id.diameterTextView) TextView diameterTextView;

    protected @BindView(R.id.vertexAndEdgesDescriptionTextView) TextView vertexAndEdgesDescriptionTextView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.drawer_legend_info, container, false);
            ButterKnife.bind(this, v);
        }

        initializeFragment();

        return v;
    }

    private void initializeFragment() {
        degreeOverallDescTextView.setText(BibleGraphApp.getContext().getResources().getString(R.string.degreeOverallDescTextView));
        degreeTextView.setText(BibleGraphApp.getContext().getResources().getString(R.string.degreeDescription));
        closenessTextView.setText(BibleGraphApp.getContext().getResources().getString(R.string.closenessDescription));
        betweennessTextView.setText(BibleGraphApp.getContext().getResources().getString(R.string.betweennessDescription));
        eigenTextView.setText(BibleGraphApp.getContext().getResources().getString(R.string.eigenDescription));
        diameterTextView.setText(BibleGraphApp.getContext().getResources().getString(R.string.diameterDescription));
        vertexAndEdgesDescriptionTextView.setText(BibleGraphApp.getContext().getResources().getString(R.string.vertexAndEdgesDescription));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i(TAG, "onDestroy()");
    }
}
