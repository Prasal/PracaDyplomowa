package pl.wat.biblegraph.app.ui.graph;

import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Shader;
import android.support.v4.content.ContextCompat;

import net.xqhs.graphs.graph.Node;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.inject.Inject;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import pl.wat.biblegraph.R;
import pl.wat.biblegraph.common.enums.GroupEnum;
import pl.wat.biblegraph.common.model.PersonRecord;
import pl.wat.biblegraph.common.model.RelationKey;
import giwi.org.networkgraph.beans.NetworkGraph;
import pl.wat.biblegraph.app.domain.shared.Data;
import pl.wat.biblegraph.app.api.CustomEdge;
import pl.wat.biblegraph.app.api.CustomNode;
import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;

/**
 * Created by Lenovo on 2017-07-20.
 */

public class GraphGenerator {

    private static final String DEMO_GROUP_NAME = GroupEnum.TRIBE_OF_ISSACHAR.getGroupName();

    @Inject
    Data data;

    private static final int COLOR_1 = ContextCompat.getColor(BibleGraphApp.getContext(), R.color.gradientVertexColor1);
    private static final int COLOR_2 = ContextCompat.getColor(BibleGraphApp.getContext(), R.color.gradientVertexColor2);

    private Map<Node, PersonRecord> nodesMap = new HashMap<>();

    public GraphGenerator(){
        ((BibleGraphApp) BibleGraphApp.getContext()).getComponent().inject(this);
    }

    public NetworkGraph createGiwiGraph(){
        NetworkGraph graph = new NetworkGraph();

        Set<RelationKey> relationSet = data.getRelationSet();

        for (RelationKey relationKey : relationSet) {
            PersonRecord primaryPerson = data.getPersonRecord(relationKey.getPrimary());
            PersonRecord relatedToPerson = data.getPersonRecord(relationKey.getRelatedTo());

            Node primaryNode = getNodeFromMap(primaryPerson);
            Node relatedToNode = getNodeFromMap(relatedToPerson);

            graph.addNode(primaryNode);
            graph.addNode(relatedToNode);

            graph.addEdge(new CustomEdge(primaryNode, relatedToNode, relationKey.getRelTypeByValue()));
        }

        return graph;
    }

    public Graph<PersonRecord, String> createDemoJungGraph(){
        Graph<PersonRecord, String> g = new UndirectedSparseGraph<>();

        Set<PersonRecord> personsFromGroup = data.getPersonsFromGroup(DEMO_GROUP_NAME);
        Set<RelationKey> relationSet = data.getRelationSet();

        int edgeIterator = 0;

        for (RelationKey relationKey : relationSet) {
            PersonRecord primary = data.getPersonRecord(relationKey.getPrimary());
            PersonRecord relatedTo = data.getPersonRecord(relationKey.getRelatedTo());

            if(personsFromGroup.contains(primary) || personsFromGroup.contains(relatedTo)){
                g.addEdge(relationKey.getRelTypeByValue().getValue() + "_" + edgeIterator++, primary, relatedTo);
            }
        }

        return g;
    }

    private Node getNodeFromMap(PersonRecord personRecord){
        if(nodesMap.values().contains(personRecord))
            return getNodeOfPerson(personRecord, nodesMap);
        else {
            Shader shader = new LinearGradient(0, 0, 3, 3, COLOR_1, COLOR_2, Shader.TileMode.MIRROR);
            Random random = new Random();

            int i = random.nextInt(180);

            Matrix matrix = new Matrix();
            matrix.setRotate(i);
            shader.setLocalMatrix(matrix);

            Node node = new CustomNode(personRecord, shader);
            nodesMap.put(node, personRecord);
            return node;
        }
    }

    private Node getNodeOfPerson(PersonRecord personRecord, Map<Node, PersonRecord> nodesMap){
        for(Node node : nodesMap.keySet())
            if (nodesMap.get(node) == personRecord)
                return node;
        return null;
    }
}
