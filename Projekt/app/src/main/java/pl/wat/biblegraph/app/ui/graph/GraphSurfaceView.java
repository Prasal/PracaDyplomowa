package pl.wat.biblegraph.app.ui.graph;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceView;
import android.widget.Toast;

import net.xqhs.graphs.graph.Edge;
import net.xqhs.graphs.graph.Node;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.inject.Inject;

import giwi.org.networkgraph.beans.Dimension;
import giwi.org.networkgraph.beans.NetworkGraph;
import giwi.org.networkgraph.beans.Point2D;
import pl.wat.biblegraph.R;
import pl.wat.biblegraph.app.api.CustomEdge;
import pl.wat.biblegraph.app.api.CustomNode;
import pl.wat.biblegraph.app.domain.shared.Data;
import pl.wat.biblegraph.app.domain.shared.event.ActiveNodeChangeEvent;
import pl.wat.biblegraph.app.domain.shared.event.RelationColouringEvent;
import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;
import pl.wat.biblegraph.app.ui.common.BibleGraphToast;
import pl.wat.biblegraph.app.giwi.FRLayout;
import pl.wat.biblegraph.common.enums.RelTypeEnum;
import pl.wat.biblegraph.common.model.PersonRecord;


public class GraphSurfaceView extends SurfaceView {

    private static final float A = 0.1f;
    private static final float B = -1f;
    private static final float C = 3f;

    private static final float CLOSENESS_TO_POINT_B = 7.5f;
    private static final float CLOSENESS_TO_POINT_A = 2.5f;

    private float CLOSENESS_FACTOR = 20;

    private ScaleGestureDetector mScaleDetector;
    private float mScaleFactor = 1.f;
    private boolean isDraggingStarted = false;

    private NetworkGraph graph;
    private FRLayout layout;

    private float currTranslationX = 0;
    private float currTranslationY = 0;

    private float startX = 0;
    private float startY = 0;

    private float oldX = 0;
    private float oldY = 0;

    private boolean isZoom = false;

    private static final int VERTEX_RADIUS = 3;
    private static final int EDGE_THICKNESS = 1;

    private Toast toast;

    @Inject
    EventBus eventBus;

    @Inject
    Data data;

    public GraphSurfaceView(Context context) {
        super(context);
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
    }

    public GraphSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
    }

    public GraphSurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RelationColouringEvent e) {
        invalidate();
    }

    public void init(NetworkGraph graph, Map<PersonRecord, Point2D> positionMap, int width, int height, boolean ifDemo) {
        this.graph = graph;
        this.layout = new FRLayout(graph, new Dimension(width, height));

        if(!ifDemo){
            for (Node node : graph.getNodes()) {
                PersonRecord personRecord = ((CustomNode) node).getPersonRecord();
                Point2D point2D = positionMap.get(personRecord);
                Point2D transform = layout.transform(node);
                transform.setLocation(point2D);
            }
        }

        getHolder().setFormat(PixelFormat.TRANSLUCENT);
        setWillNotDraw(false);

        ((BibleGraphApp) BibleGraphApp.getContext()).getComponent().inject(this);
    }

    private void drawGraph(Canvas canvas) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(EDGE_THICKNESS);

        Set<Edge> edgesNotCoincidentToActiveNode = new HashSet<>(graph.getEdges());
        Set<Edge> edgesCoincidentToActiveNode = new HashSet<>();

        if(data.getActiveNode() != null){
            for (Edge edge : graph.getEdges()) {
                if(edge.getFrom() == data.getActiveNode() || edge.getTo() == data.getActiveNode())
                    edgesCoincidentToActiveNode.add(edge);
            }
        }

        edgesNotCoincidentToActiveNode.removeAll(edgesCoincidentToActiveNode);

        drawNotCoincidentEdges(canvas, paint, edgesNotCoincidentToActiveNode);
        drawCoincidentEdges(canvas, paint, edgesCoincidentToActiveNode);

        Set<Node> set = new HashSet<>(graph.getNodes());
        set.remove(data.getActiveNode());

        drawUnactiveNodes(canvas, paint, set);
        drawActiveNode(canvas, paint);
    }

    private void drawCoincidentEdges(Canvas canvas, Paint paint, Set<Edge> edgesCoincidentToActiveNode) {
        for (Edge edge : edgesCoincidentToActiveNode) {
            Point2D p1 = layout.transform(edge.getFrom());
            Point2D p2 = layout.transform(edge.getTo());

            RelTypeEnum relTypeEnum = ((CustomEdge) edge).getRelTypeEnum();
            int edgeColor = getColorByRelType(relTypeEnum, data.isRelationsColouring(), true);
            paint.setColor(edgeColor);

            PointF e1 = new PointF((float) p1.getX(), (float) p1.getY());
            PointF e2 = new PointF((float) p2.getX(), (float) p2.getY());

            canvas.drawLine(e1.x, e1.y, e2.x, e2.y, paint);
        }
    }

    private void drawNotCoincidentEdges(Canvas canvas, Paint paint, Set<Edge> edgesNotCoincidentToActiveNode) {
        for (Edge edge : edgesNotCoincidentToActiveNode) {
            Point2D p1 = layout.transform(edge.getFrom());
            Point2D p2 = layout.transform(edge.getTo());

            RelTypeEnum relTypeEnum = ((CustomEdge) edge).getRelTypeEnum();
            int edgeColor = getColorByRelType(relTypeEnum, data.isRelationsColouring(), false);
            paint.setColor(edgeColor);

            PointF e1 = new PointF((float) p1.getX(), (float) p1.getY());
            PointF e2 = new PointF((float) p2.getX(), (float) p2.getY());

            canvas.drawLine(e1.x, e1.y, e2.x, e2.y, paint);
        }
    }

    private void drawUnactiveNodes(Canvas canvas, Paint paint, Collection<Node> nodes) {
        for (Node node : nodes) {
            Point2D position = layout.transform(node);
            CustomNode customNode = (CustomNode) node;
            paint.setShader(customNode.getShader());
            canvas.drawCircle((float) position.getX(), (float) position.getY(), VERTEX_RADIUS, paint);
        }
    }

    private void drawActiveNode(Canvas canvas, Paint paint) {
        if(data.getActiveNode() != null){
            Point2D position = layout.transform(data.getActiveNode());
            paint.setColor(ContextCompat.getColor(BibleGraphApp.getContext(), R.color.activatedVertexColor));
            paint.setShader(null);
            canvas.drawCircle((float) position.getX(), (float) position.getY(), VERTEX_RADIUS, paint);
        }
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent ev) {
        if(!isZoom && ev.getAction() == MotionEvent.ACTION_DOWN){
            startX = ev.getX();
            startY = ev.getY();
        }
        else if(!isZoom && ev.getAction() == MotionEvent.ACTION_MOVE){
            if(!isDraggingStarted){
                oldX = oldX + currTranslationX;
                oldY = oldY + currTranslationY;
                isDraggingStarted = true;
            }

            float deltaX = ev.getX() - startX;
            float deltaY = ev.getY() - startY;

            float factor = (float) (A * Math.pow(mScaleFactor, 2)  + B * mScaleFactor + C);

            currTranslationX = deltaX * factor;
            currTranslationY = deltaY * factor;

            invalidate();
        }
        else if(ev.getAction() == MotionEvent.ACTION_UP){
            if(!isDraggingStarted){
                activateNode(ev);
            }

            isDraggingStarted = false;
            isZoom = false;
        }

        mScaleDetector.onTouchEvent(ev);
        return true;
    }

    private void activateNode(MotionEvent ev){
        List<Node> list = new ArrayList<>();

        for(Node node : graph.getNodes()){
            Point2D point = layout.transform(node);
            if(isCloseToPoint(point, ev))
                list.add(node);
        }

        if(list.size() > 0){
            Random random = new Random();
            int rand = random.nextInt(list.size());
            data.setActiveNode( (CustomNode) list.get(rand) );
            eventBus.postSticky(new ActiveNodeChangeEvent());
            invalidate();
        }
    }

    @Override
    public void onDraw(Canvas canvas){
        canvas.scale(mScaleFactor, mScaleFactor);

        canvas.translate(oldX, oldY);
        canvas.translate(currTranslationX, currTranslationY);

        drawGraph(canvas);
    }

    private boolean isCloseToPoint(Point2D point, MotionEvent ev){
        float xClick = ev.getX();
        float yClick = ev.getY();

        return Math.abs((point.getX() + currTranslationX + oldX) * mScaleFactor - xClick) < CLOSENESS_FACTOR
                && Math.abs((point.getY() + currTranslationY + oldY) * mScaleFactor - yClick) < CLOSENESS_FACTOR;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 5.0f));

            CLOSENESS_FACTOR = CLOSENESS_TO_POINT_A * mScaleFactor + CLOSENESS_TO_POINT_B;

            isZoom = true;
            invalidate();

            return true;
        }
    }

    public void center(){
        mScaleFactor = 1.f;
        currTranslationX = 0;
        currTranslationY = 0;
        startX = 0;
        startY = 0;
        oldX = 0;
        oldY = 0;

        invalidate();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ActiveNodeChangeEvent e) {
        if(toast != null)
            toast.cancel();

        String text = getNameAndSurname(data.getActiveNode().getPersonRecord());

        toast = BibleGraphToast.prepareToast(text, Toast.LENGTH_SHORT);
        toast.show();
    }

    private String getNameAndSurname(PersonRecord personRecord){
        return personRecord.getSurname().equals("-") ? personRecord.getName() : personRecord.getName() + " " +  personRecord.getSurname();
    }

    public int getColorByRelType(RelTypeEnum relTypeEnum, boolean relationsColouring, boolean isCoincidentWithActiveNode){
        if(data.getActiveNode() == null && relationsColouring)
            return getColorByRelType(relTypeEnum);
        else if (data.getActiveNode() == null && !relationsColouring)
            return ContextCompat.getColor(BibleGraphApp.getContext(), R.color.normalEdge);
        else if(data.getActiveNode() != null && isCoincidentWithActiveNode && relationsColouring)
            return getColorByRelType(relTypeEnum);
        else if(data.getActiveNode() != null && isCoincidentWithActiveNode && !relationsColouring)
            return ContextCompat.getColor(BibleGraphApp.getContext(), R.color.activatedEdge);
        else
            return ContextCompat.getColor(BibleGraphApp.getContext(), R.color.normalEdge);
    }

    private int getColorByRelType(RelTypeEnum relTypeEnum) {
        if(relTypeEnum == RelTypeEnum.PARENT_CHILD)
            return ContextCompat.getColor(BibleGraphApp.getContext(), R.color.parentChildColor);
        else if(relTypeEnum == RelTypeEnum.SIBLING)
            return ContextCompat.getColor(BibleGraphApp.getContext(), R.color.siblingColor);
        else if(relTypeEnum == RelTypeEnum.SPOUSE)
            return ContextCompat.getColor(BibleGraphApp.getContext(), R.color.spouseColor);
        else
            return Color.BLACK;
    }
}
