package pl.wat.biblegraph.app.domain.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import javax.inject.Inject;

import pl.wat.biblegraph.R;
import pl.wat.biblegraph.app.domain.shared.Data;
import pl.wat.biblegraph.app.domain.shared.enums.SharedPreferenceCode;
import pl.wat.biblegraph.app.ui.fragment.MainFragment;
import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;
import pl.wat.biblegraph.app.domain.service.SharedPreferencesService;


public class MainActivity extends FragmentActivity {

    @Inject
    SharedPreferencesService sharedPreferencesService;

    @Inject
    Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((BibleGraphApp) getApplication()).getComponent().inject(this);

        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, new MainFragment())
                .commit();

        ((BibleGraphApp) getApplication()).getComponent().inject(this);

        data.setRelationsColouring( sharedPreferencesService.readBoolean(SharedPreferenceCode.RELATIONS_COLOURING.getPref()) );
    }
}
