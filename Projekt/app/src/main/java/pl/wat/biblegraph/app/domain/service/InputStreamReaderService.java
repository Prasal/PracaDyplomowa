package pl.wat.biblegraph.app.domain.service;

import java.io.IOException;
import java.io.InputStreamReader;

import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;

/**
 * Created by Lenovo on 2017-11-28.
 */

public class InputStreamReaderService {
    public static InputStreamReader getInputStreamReader(String filename){
        InputStreamReader inputStreamReader = null;

        try {
            inputStreamReader = new InputStreamReader(BibleGraphApp.getContext().getAssets().open(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return inputStreamReader;
    }
}
