package pl.wat.biblegraph.app.infrastructure.injection;


import javax.inject.Singleton;

import dagger.Component;
import pl.wat.biblegraph.app.domain.activity.GraphActivity;
import pl.wat.biblegraph.app.domain.activity.MainActivity;
import pl.wat.biblegraph.app.ui.fragment.GraphInfoFragment;
import pl.wat.biblegraph.app.ui.fragment.OptionsFragment;
import pl.wat.biblegraph.app.ui.fragment.VertexInfoFragment;
import pl.wat.biblegraph.app.ui.fragment.GraphFragment;
import pl.wat.biblegraph.app.ui.fragment.MainFragment;
import pl.wat.biblegraph.app.ui.graph.GraphGenerator;
import pl.wat.biblegraph.app.ui.graph.GraphSurfaceView;


@Singleton
@Component(modules = {BibleGraphModule.class, EventBusModule.class})
public interface BibleGraphComponent {
    void inject(MainActivity mainActivity);
    void inject(GraphGenerator graphGenerator);
    void inject(GraphActivity graphActivity);
    void inject(GraphFragment graphFragment);
    void inject(MainFragment mainFragment);
    void inject(GraphSurfaceView graphSurfaceView);
    void inject(VertexInfoFragment vertexInfoFragment);
    void inject(GraphInfoFragment vertexInfoFragment);
    void inject(OptionsFragment optionsFragment);
}