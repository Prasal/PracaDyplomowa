package pl.wat.biblegraph.app.infrastructure.repository;

import pl.wat.biblegraph.common.enums.GroupEnum;
import pl.wat.biblegraph.common.enums.LayoutEnum;
import pl.wat.biblegraph.common.model.ResponseArray;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface RepositoryService {
    @GET
    Call<ResponseArray> getGraph(@Url String baseUrl,
                                 @Query("width") int width,
                                 @Query("height") int height,
                                 @Query("group") GroupEnum groupEnum,
                                 @Query("layout") LayoutEnum layout);
}