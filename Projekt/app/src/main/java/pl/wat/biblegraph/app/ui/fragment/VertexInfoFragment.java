package pl.wat.biblegraph.app.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.wat.biblegraph.R;
import pl.wat.biblegraph.app.domain.shared.event.ActiveNodeChangeEvent;
import pl.wat.biblegraph.common.model.PersonRecord;
import pl.wat.biblegraph.app.domain.shared.ApiTools;
import pl.wat.biblegraph.app.domain.shared.Data;
import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;


public class VertexInfoFragment extends Fragment {

    @Inject
    Data data;

    @Inject
    ApiTools apiTools;

    @Inject
    EventBus eventBus;

    private final String TAG = this.getClass().getSimpleName();

    private View v;

    protected @BindView(R.id.vertexNameTextView) TextView vertexNameTextView;
    protected @BindView(R.id.vertexGenderTextView) TextView vertexGenderTextView;
    protected @BindView(R.id.vertexBirthPlaceTextView) TextView vertexBirthPlaceTextView;
    protected @BindView(R.id.vertexDeathPlaceTextView) TextView vertexDeathPlaceTextView;
    protected @BindView(R.id.vertexBirthYearTextView) TextView vertexBirthYearTextView;
    protected @BindView(R.id.vertexDeathYearTextView) TextView vertexDeathYearTextView;

    protected @BindView(R.id.vertexDegreeTextView) TextView vertexDegreeTextView;
    protected @BindView(R.id.vertexClosenessTextView) TextView vertexClosenessTextView;
    protected @BindView(R.id.vertexBetweennessTextView) TextView vertexBetweennessTextView;
    protected @BindView(R.id.vertexEigenTextView) TextView vertexEigenTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.drawer_vertex_info, container, false);
            ButterKnife.bind(this, v);
        }

        ((BibleGraphApp) getActivity().getApplication()).getComponent().inject(this);

        return v;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(ActiveNodeChangeEvent e) {
        updateDataOnDrawer(data.getActiveNode().getPersonRecord());
    }

    private void updateDataOnDrawer(PersonRecord personRecord){
        vertexNameTextView.setText(getNameAndSurname(personRecord));
        vertexGenderTextView.setText(getGender(personRecord));
        vertexBirthPlaceTextView.setText(getPlaceOrYear(personRecord.getBirthPlace()));
        vertexDeathPlaceTextView.setText(getPlaceOrYear(personRecord.getDeathPlace()));
        vertexBirthYearTextView.setText(getPlaceOrYear(personRecord.getBirthYear()));
        vertexDeathYearTextView.setText(getPlaceOrYear(personRecord.getDeathYear()));

        Map<String, Number> measureMap = data.getMeasureMap(personRecord);

        vertexDegreeTextView.setText(apiTools.measureToString(measureMap.get("degree")));
        vertexClosenessTextView.setText(apiTools.measureToString(measureMap.get("closeness")));
        vertexBetweennessTextView.setText(apiTools.measureToString(measureMap.get("betweenness")));
        vertexEigenTextView.setText(apiTools.measureToString(measureMap.get("eigen")));
    }

    private String getPlaceOrYear(String placeOrYear){
        return placeOrYear.equals("-") ? "(brak danych)" : placeOrYear;
    }

    private String getNameAndSurname(PersonRecord personRecord){
        return personRecord.getSurname().equals("-") ? personRecord.getName() : personRecord.getName() + " " +  personRecord.getSurname();
    }

    private String getGender(PersonRecord personRecord){
        String gender = personRecord.getGender();

        if(gender.equals("M"))
            return "Mężczyzna";
        else if(gender.equals("F"))
            return "Kobieta";
        else
            return "";
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);
        Log.i(TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(this);
        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
    }
}
