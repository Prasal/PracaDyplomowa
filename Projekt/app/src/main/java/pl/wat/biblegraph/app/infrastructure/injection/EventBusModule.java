package pl.wat.biblegraph.app.infrastructure.injection;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lenovo on 2017-09-03.
 */

@Module
public class EventBusModule {
    private static EventBus eventBus;

    @Provides
    @Singleton
    public EventBus provideEventBus() {
        return getEventBusInstance();
    }

    private static EventBus getEventBusInstance(){
        if(eventBus == null){
            eventBus = EventBus.builder().logNoSubscriberMessages(true).sendNoSubscriberEvent(false).build();
        }
        return eventBus;
    }
}

