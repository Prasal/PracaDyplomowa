package pl.wat.biblegraph.app.api;

import net.xqhs.graphs.graph.Node;
import net.xqhs.graphs.graph.SimpleEdge;

import pl.wat.biblegraph.common.enums.RelTypeEnum;
import lombok.Getter;

@Getter
public class CustomEdge extends SimpleEdge {
    private RelTypeEnum relTypeEnum;

    public CustomEdge(Node fromNode, Node toNode, RelTypeEnum relTypeEnum) {
        super(fromNode,toNode, "");
        this.relTypeEnum = relTypeEnum;
    }
}