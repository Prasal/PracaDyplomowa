package pl.wat.biblegraph.app.domain.shared.enums;

import lombok.Getter;

/**
 * Created by Lenovo on 2017-10-18.
 */

@Getter
public enum SharedPreferenceCode {
    RELATIONS_COLOURING("relations_colouring");

    private final String pref;

    SharedPreferenceCode(String pref) {
        this.pref = pref;
    }
}
