package pl.wat.biblegraph.app.domain.shared;

import java.util.Formatter;

import pl.wat.biblegraph.R;
import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;


/**
 * Created by Lenovo on 2017-07-11.
 */

public class ApiTools {
    private static final int MAX_NUMBER_OF_DIGITS_AFTER_DOT = 7;

    public String measureToString(Number x){
        if(x.intValue() == -1)
            return BibleGraphApp.getContext().getResources().getString(R.string.infinity);

        double wholeNumber = x.doubleValue();
        long beforePoint = Math.round(wholeNumber);
        double afterPoint = wholeNumber - Math.round(wholeNumber);

        double pow = Math.pow(10, MAX_NUMBER_OF_DIGITS_AFTER_DOT);

        double v = beforePoint + (double) Math.round(afterPoint * pow) / pow;

        StringBuilder sbuf = new StringBuilder();
        Formatter fmt = new Formatter(sbuf);
        fmt.format("%." + MAX_NUMBER_OF_DIGITS_AFTER_DOT + "f", v);
        String s = sbuf.toString();
        s = s.replace(".", ",");

        int j = s.length()-1;

        while (s.charAt(j) == '0' && s.charAt(j-1) == '0' || s.charAt(j-1) == ','){
            j--;
        }

        return s.substring(0, j);
    }
}
