package pl.wat.biblegraph.app.domain.shared.event;

import lombok.Data;

/**
 * Created by Lenovo on 2017-09-03.
 */

@Data
public class RelationColouringEvent {
    private boolean isRelationColouring;

    public RelationColouringEvent(boolean isRelationColouring) {
        this.isRelationColouring = isRelationColouring;
    }
}

