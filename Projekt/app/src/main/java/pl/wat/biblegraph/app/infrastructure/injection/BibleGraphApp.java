package pl.wat.biblegraph.app.infrastructure.injection;

import android.app.Application;
import android.content.Context;

public class BibleGraphApp extends Application {

    public static final String APPLICATION_PACKAGE_NAME = "pl.wat.biblegraph";
    private BibleGraphComponent component;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerBibleGraphComponent.Builder builder = DaggerBibleGraphComponent.builder()
                .bibleGraphModule(new BibleGraphModule())
                .eventBusModule(new EventBusModule());

        this.component = builder.build();
        this.context = getApplicationContext();
    }

    public BibleGraphComponent getComponent() {
        return component;
    }

    public static Context getContext() {
        return context;
    }
}
