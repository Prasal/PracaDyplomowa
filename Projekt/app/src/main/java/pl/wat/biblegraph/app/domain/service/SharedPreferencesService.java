package pl.wat.biblegraph.app.domain.service;

import android.content.Context;
import android.os.AsyncTask;

import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;

/**
 * Created by Lenovo on 2017-10-18.
 */

public class SharedPreferencesService {
    public void writeAsync(String key, Object value) {
        new WriteSharedPreferenceTaskForString().execute(key, value);
    }

    class WriteSharedPreferenceTaskForString extends AsyncTask<Object, Integer, Void> {
        @Override
        protected Void doInBackground(Object... strings) {
            BibleGraphApp.getContext()
                    .getSharedPreferences(BibleGraphApp.APPLICATION_PACKAGE_NAME, Context.MODE_PRIVATE)
                    .edit()
                    .putString(strings[0].toString(), strings[1].toString())
                    .apply();

            return null;
        }
    }

    public boolean readBoolean(String key) {
        return Boolean.valueOf(read(key));
    }

    private String read(String key) {
        return BibleGraphApp.getContext()
                .getSharedPreferences(BibleGraphApp.APPLICATION_PACKAGE_NAME, Context.MODE_PRIVATE)
                .getString(key, null);
    }
}
