package pl.wat.biblegraph.app.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.wat.biblegraph.R;
import pl.wat.biblegraph.app.domain.shared.Data;
import pl.wat.biblegraph.app.domain.shared.enums.SharedPreferenceCode;
import pl.wat.biblegraph.app.domain.shared.event.RelationColouringEvent;
import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;
import pl.wat.biblegraph.app.domain.service.SharedPreferencesService;


public class OptionsFragment extends Fragment {

    @Inject
    Data data;

    @Inject
    EventBus eventBus;

    @Inject
    SharedPreferencesService sharedPreferencesService;

    private final String TAG = this.getClass().getSimpleName();
    private View v;

    protected @BindView(R.id.relationsColouringCheckBox) CheckBox relationsColouringCheckBox;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.drawer_options, container, false);
            ButterKnife.bind(this, v);
        }

        ((BibleGraphApp) getActivity().getApplication()).getComponent().inject(this);

        initializeView();

        return v;
    }

    private void initializeView() {
        relationsColouringCheckBox.setOnCheckedChangeListener(getRelationColouringCheckboxListener());
        relationsColouringCheckBox.setChecked(data.isRelationsColouring());
    }

    @NonNull
    private CompoundButton.OnCheckedChangeListener getRelationColouringCheckboxListener() {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                data.setRelationsColouring(isChecked);
                eventBus.post(new RelationColouringEvent(isChecked));
                sharedPreferencesService.writeAsync(SharedPreferenceCode.RELATIONS_COLOURING.getPref(), isChecked);
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i(TAG, "onDestroy()");
    }
}
