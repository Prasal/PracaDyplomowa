package pl.wat.biblegraph.app.ui.fragment;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Pair;
import pl.wat.biblegraph.R;
import pl.wat.biblegraph.app.domain.service.InputStreamReaderService;
import pl.wat.biblegraph.common.shared.LoaderImpl;
import pl.wat.biblegraph.common.shared.Measure;
import pl.wat.biblegraph.common.model.MaxCentralityDto;
import pl.wat.biblegraph.common.model.ResponseArray;
import pl.wat.biblegraph.common.model.ResponseEdgeDto;
import pl.wat.biblegraph.common.model.ResponseGraphInfoDto;
import pl.wat.biblegraph.common.model.ResponseVertexDto;
import pl.wat.biblegraph.common.shared.CommonTools;
import pl.wat.biblegraph.common.model.PersonRecord;
import giwi.org.networkgraph.beans.NetworkGraph;
import pl.wat.biblegraph.app.domain.shared.Data;
import pl.wat.biblegraph.app.ui.common.SectionPagerAdapter;
import pl.wat.biblegraph.app.ui.graph.GraphGenerator;
import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;
import pl.wat.biblegraph.app.ui.graph.GraphSurfaceView;


public class GraphFragment extends Fragment {

    @Inject
    Data data;

    @Inject
    EventBus eventBus;

    @Inject
    Measure measure;

    private final String TAG = this.getClass().getSimpleName();
    private static final int FADING_AWAY_TIME = 1000;
    private static final int VISIBILITY_TIME = 1000;

    private CountDownTimer countDownTimer;
    private View v;

    protected @BindView(R.id.center_icon) ImageView centerIconImageView;
    protected @BindView(R.id.show_menu_icon) ImageView showMenuIconImageView;
    protected @BindView(R.id.left_arrow_icon) ImageView leftArrowIconImageView;

    protected @BindView(R.id.drawer_layout) DrawerLayout drawer;
    protected @BindView(R.id.drawerBody) LinearLayout drawerLayout;
    protected @BindView(R.id.mysurface) GraphSurfaceView surface;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_graph, container, false);
            ButterKnife.bind(this, v);
        }

        ((BibleGraphApp) getActivity().getApplication()).getComponent().inject(this);

        int width = getActivity().getIntent().getIntExtra("width", 0);
        int height = getActivity().getIntent().getIntExtra("height", 0);
        boolean ifDemo = getActivity().getIntent().getBooleanExtra("ifDemo", true);

        showGraph(width, height, ifDemo);

        showMenuIcon();

        surface.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(countDownTimer != null)
                    countDownTimer.cancel();

                countDownTimer = new CountDownTimer(FADING_AWAY_TIME + VISIBILITY_TIME, FADING_AWAY_TIME + VISIBILITY_TIME) {
                    public void onTick(long millisUntilFinished) {}

                    public void onFinish() {
                        fadeAwayImage(showMenuIconImageView);
                        fadeAwayImage(centerIconImageView);
                        fadeAwayImage(leftArrowIconImageView);
                    }
                }.start();

                if(showMenuIconImageView.getAnimation() != null)
                    showMenuIconImageView.getAnimation().cancel();

                if(centerIconImageView.getAnimation() != null)
                    centerIconImageView.getAnimation().cancel();

                if(leftArrowIconImageView.getAnimation() != null)
                    leftArrowIconImageView.getAnimation().cancel();

                showMenuIconImageView.setVisibility(View.VISIBLE);
                centerIconImageView.setVisibility(View.VISIBLE);
                leftArrowIconImageView.setVisibility(View.VISIBLE);

                return false;
            }
        });

        ViewPager viewPager = (ViewPager) v.findViewById(R.id.pager);
        viewPager.setAdapter(new SectionPagerAdapter(getActivity().getSupportFragmentManager()));

        return v;
    }

    private void showMenuIcon() {
        centerIconImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    surface.center();
                }

                return true;
            }
        });

        showMenuIconImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    drawer.openDrawer(drawerLayout);
                }

                return true;
            }
        });

        leftArrowIconImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    getActivity().onBackPressed();
                }

                return true;
            }
        });
    }

    private void fadeAwayImage(final ImageView iv){
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(FADING_AWAY_TIME);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                iv.setVisibility(View.INVISIBLE);
            }

            public void onAnimationRepeat(Animation animation) {}

            public void onAnimationStart(Animation animation) {}
        });

        iv.startAnimation(fadeOut);
    }

    private void showGraph(int width, int height, boolean ifDemo){
        GraphGenerator graphGenerator = new GraphGenerator();

        if(ifDemo){
            LoaderImpl demoLoader = new LoaderImpl(data);
            demoLoader.init(
                    InputStreamReaderService.getInputStreamReader("People.csv"),
                    InputStreamReaderService.getInputStreamReader("PeopleRelationships.csv"),
                    InputStreamReaderService.getInputStreamReader("PeopleGroups.csv")
            );

            Graph<PersonRecord, String> jungGraph = graphGenerator.createDemoJungGraph();
            ResponseArray responseArray = prepareResponse(jungGraph);
            data.loadFromResponseArray(responseArray, true);
        }

        NetworkGraph giwiGraph = graphGenerator.createGiwiGraph();
        surface.init(giwiGraph, data.getPositionMap(), width, height, ifDemo);
    }

    private ResponseArray prepareResponse(Graph<PersonRecord, String> jungGraph){
        List<ResponseVertexDto> responseVertexDtos = new ArrayList<>();
        List<ResponseEdgeDto> responseEdgeDtos = new ArrayList<>();

        Map<String, Number> variousMeasuresForGraphMap = measure.measureDiameter(jungGraph);
        Map<Integer, Map<String, Number>> centralityForVerticesMap = measure.measureCentrality(jungGraph);
        Map<String, MaxCentralityDto> maxCentralitiesForGraphMap = measure.measureMax(centralityForVerticesMap);

        for (PersonRecord personRecord : jungGraph.getVertices()) {
            ResponseVertexDto responseVertexDto = new ResponseVertexDto();
            responseVertexDto.setPersonRecord(personRecord);
            responseVertexDto.setMeasuresMap( centralityForVerticesMap.get(personRecord.getId()) );
            responseVertexDtos.add(responseVertexDto);
        }

        for (String s : jungGraph.getEdges()) {
            Pair<PersonRecord> endpoints = jungGraph.getEndpoints(s);

            ResponseEdgeDto responseEdgeDto = new ResponseEdgeDto();
            responseEdgeDto.setPrimary(endpoints.getFirst());
            responseEdgeDto.setRelatedTo(endpoints.getSecond());
            responseEdgeDto.setRelType(CommonTools.getRelTypeByValue(s.substring(0, s.indexOf("_"))));

            responseEdgeDtos.add(responseEdgeDto);
        }

        ResponseGraphInfoDto responseGraphInfoDto = new ResponseGraphInfoDto();
        responseGraphInfoDto.setVariousMeasuresForGraph(variousMeasuresForGraphMap);
        responseGraphInfoDto.setMaxCentralitiesForGraphMap(maxCentralitiesForGraphMap);

        ResponseArray responseArray = new ResponseArray();
        responseArray.setResponseVertexDtos(responseVertexDtos);
        responseArray.setResponseEdgeDtos(responseEdgeDtos);
        responseArray.setResponseGraphInfoDto(responseGraphInfoDto);

        return responseArray;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onStart() {
        super.onStart();
        eventBus.register(surface);
        Log.i(TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(surface);
        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
    }
}
