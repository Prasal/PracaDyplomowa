package pl.wat.biblegraph.app.domain.shared;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.wat.biblegraph.common.shared.CommonData;
import pl.wat.biblegraph.common.model.MaxCentralityDto;
import pl.wat.biblegraph.common.model.PersonRecord;
import pl.wat.biblegraph.common.model.RelationKey;
import pl.wat.biblegraph.common.model.ResponseArray;
import pl.wat.biblegraph.common.model.ResponseEdgeDto;
import pl.wat.biblegraph.common.model.ResponseGraphInfoDto;
import pl.wat.biblegraph.common.model.ResponseVertexDto;
import giwi.org.networkgraph.beans.Point2D;
import lombok.Setter;
import pl.wat.biblegraph.app.api.CustomNode;


/**
 * Created by Lenovo on 2017-06-27.
 */

@Setter
public class Data extends CommonData {
    private Map<PersonRecord, Point2D> positionMap = new HashMap<>();
    private Map<PersonRecord, Map<String, Number>> measuresForVerticesMap = new HashMap<>();
    private Map<String, Number> variousMeasuresForGraph;
    private Map<String, MaxCentralityDto> maxCentralitiesForGraphMap;
    private CustomNode activeNode;
    private boolean isRelationsColouring = false;

    public void loadFromResponseArray(ResponseArray body, boolean ifDemo){
        personRecordMap.clear();
        relationSet.clear();
        positionMap.clear();
        measuresForVerticesMap.clear();

        List<ResponseVertexDto> responseVertexDtos = body.getResponseVertexDtos();
        List<ResponseEdgeDto> responseEdgeDtos = body.getResponseEdgeDtos();

        for (ResponseVertexDto responseVertexDto : responseVertexDtos) {
            PersonRecord personRecord = responseVertexDto.getPersonRecord();
            Map<String, Number> measuresMap = responseVertexDto.getMeasuresMap();

            if(!ifDemo){
                Point2D point2D = new Point2D();
                point2D.setLocation(responseVertexDto.getPoint().getX(), responseVertexDto.getPoint().getY());
                positionMap.put(personRecord, point2D);
            }

            personRecordMap.put(personRecord.getId(), personRecord);
            measuresForVerticesMap.put(personRecord, measuresMap);
        }

        for (ResponseEdgeDto responseEdgeDto : responseEdgeDtos) {
            PersonRecord primary = responseEdgeDto.getPrimary();
            PersonRecord relatedTo = responseEdgeDto.getRelatedTo();

            RelationKey relationKey = new RelationKey();
            relationKey.setPrimary(primary.getId());
            relationKey.setRelatedTo(relatedTo.getId());
            relationKey.setRelTypeByValue(responseEdgeDto.getRelType());

            relationSet.add(relationKey);
        }

        ResponseGraphInfoDto responseGraphInfoDto = body.getResponseGraphInfoDto();
        variousMeasuresForGraph = responseGraphInfoDto.getVariousMeasuresForGraph();
        maxCentralitiesForGraphMap = responseGraphInfoDto.getMaxCentralitiesForGraphMap();
    }

    public CustomNode getActiveNode() {
        return activeNode;
    }

    public Number getMeasureForGraph(String key) {
        return variousMeasuresForGraph.get(key);
    }

    public MaxCentralityDto getMaxCentraliyForGraph(String key) {
        return maxCentralitiesForGraphMap.get(key);
    }

    public Map<PersonRecord, Point2D> getPositionMap(){
        return positionMap;
    }

    public Map<String, Number> getMeasureMap(PersonRecord personRecord){
        return measuresForVerticesMap.get(personRecord);
    }


    public boolean isRelationsColouring() {
        return isRelationsColouring;
    }
}
