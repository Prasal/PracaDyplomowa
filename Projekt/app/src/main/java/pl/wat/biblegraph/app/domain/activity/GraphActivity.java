package pl.wat.biblegraph.app.domain.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import pl.wat.biblegraph.R;
import pl.wat.biblegraph.app.domain.shared.Data;
import pl.wat.biblegraph.app.ui.fragment.GraphFragment;
import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;


public class GraphActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    @Inject
    EventBus eventBus;

    @Inject
    Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((BibleGraphApp) getApplication()).getComponent().inject(this);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_graph);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.graph_frame, new GraphFragment())
                .commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        data.setActiveNode(null);
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
    }
}
