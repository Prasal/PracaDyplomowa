package pl.wat.biblegraph.app.ui.fragment;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.wat.biblegraph.R;
import pl.wat.biblegraph.app.domain.activity.GraphActivity;
import pl.wat.biblegraph.app.domain.shared.Data;
import pl.wat.biblegraph.app.infrastructure.injection.BibleGraphApp;
import pl.wat.biblegraph.app.infrastructure.repository.RepositoryService;
import pl.wat.biblegraph.app.ui.common.BibleGraphToast;
import pl.wat.biblegraph.common.enums.GroupEnum;
import pl.wat.biblegraph.common.enums.LayoutEnum;
import pl.wat.biblegraph.common.model.ResponseArray;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment {

    @Inject
    RepositoryService repositoryService;

    @Inject
    Data data;

    protected @BindView(R.id.layout_type_spinner) Spinner layoutTypeSpinner;
    protected @BindView(R.id.group_type_spinner) Spinner groupTypeSpinner;
    protected @BindView(R.id.button) Button btn;
    protected @BindView(R.id.loading_text) TextView loadingText;

    private int width;
    private int height;
    private final String TAG = this.getClass().getSimpleName();

    private View v;
    private boolean isServerWorkingNow = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_main, container, false);
            ButterKnife.bind(this, v);
        }

        ((BibleGraphApp) getActivity().getApplication()).getComponent().inject(this);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        width = size.x;
        height = size.y;

        setLayoutSpinner();
        setGroupSpinner();
        setButton();

        return v;
    }

    private void showDemo() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                startGraphActivity(width, height, true);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setServerWorking(false);
                    }
                });
            }
        }).start();
    }

    private void callService(LayoutEnum layoutEnum, GroupEnum groupEnum){
        String baseUrl = handleUrlFile();

        if(baseUrl.equals("ERROR")){
            showErrorToast(R.string.file_creation_attempt_failed);
            setServerWorking(false);
            return;
        }
        else if(baseUrl.equals("EMPTY")){
            showErrorToast(R.string.file_is_empty);
            setServerWorking(false);
            return;
        }

        String url = baseUrl + "/getGraph";
        Call<ResponseArray> call = repositoryService.getGraph(url, width, height, groupEnum, layoutEnum);
        call.enqueue(new Callback<ResponseArray>() {

            @Override
            public void onResponse(Call<ResponseArray> call, Response<ResponseArray> response) {
                ResponseArray body = response.body();

                if(body == null){
                    onFailure(call, new NullPointerException("Wrong endpoint error"));
                    return;
                }

                data.loadFromResponseArray(body, false);
                startGraphActivity(width, height, false);
                Log.i(TAG, response.message());
                setServerWorking(false);
            }

            @Override
            public void onFailure(Call<ResponseArray> call, Throwable t) {
                Log.i(TAG, t.getMessage());
                setServerWorking(false);

                String errorText;

                if(t.getClass().equals(SocketTimeoutException.class))
                    errorText = BibleGraphApp.getContext().getResources().getString(R.string.cannot_connect);
                else
                    errorText = BibleGraphApp.getContext().getResources().getString(R.string.unknown_error_occured);

                BibleGraphToast.prepareToast(errorText, Toast.LENGTH_LONG).show();
            }
        });
    }

    private String handleUrlFile() {
        File sdcard = Environment.getExternalStorageDirectory();
        File dir = new File(sdcard.getAbsolutePath() + "/BibleGraph");
        File file = new File(dir, "url.txt");
        String baseUrl;

        if(dir.exists() && file.exists()){
            baseUrl = handleUrlFileIfExists(file);
        }
        else {
            baseUrl = handleFileIfNotExists(dir, file);
        }

        if(baseUrl.endsWith("/"))
            baseUrl = baseUrl.substring(0, baseUrl.length()-1);

        if(baseUrl.startsWith(""))
            baseUrl = baseUrl.substring(1);

        return baseUrl;
    }

    @NonNull
    private String handleFileIfNotExists(File dir, File file) {
        String baseUrl;
        dir.mkdir();

        String defaultBaseUrl = "http://www.example.com";
        baseUrl = defaultBaseUrl;

        FileOutputStream os = null;
        try {
            os = new FileOutputStream(file);
            os.write(defaultBaseUrl.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            baseUrl = "ERROR";
        }

        try {
            if(os != null)
                os.close();
        } catch (IOException e) {
            e.printStackTrace();
            baseUrl = "ERROR";
        }
        return baseUrl;
    }

    @NonNull
    private String handleUrlFileIfExists(File file) {
        String baseUrl;
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
            }

            br.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            showErrorToast(R.string.unknown_error_occured);
        }

        if(text.toString().isEmpty())
            baseUrl = "EMPTY";
        else
            baseUrl = text.toString();
        return baseUrl;
    }

    private void showErrorToast(int msgRes) {
        String errorText = BibleGraphApp.getContext().getResources().getString(msgRes);
        BibleGraphToast.prepareToast(errorText, Toast.LENGTH_LONG).show();
    }

    private void startGraphActivity(int width, int height, boolean ifDemo) {
        Intent intent = new Intent(getActivity(), GraphActivity.class);

        intent.putExtra("width", width);
        intent.putExtra("height", height);
        intent.putExtra("ifDemo", ifDemo);

        startActivity(intent);
    }

    private void setButton() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isServerWorkingNow){
                    setServerWorking(true);

                    LayoutEnum layoutEnum = null;
                    GroupEnum groupEnum = null;

                    String layoutEnumStr = layoutTypeSpinner.getSelectedItem().toString();
                    String groupEnumStr = groupTypeSpinner.getSelectedItem().toString();

                    for (LayoutEnum l : LayoutEnum.values()) {
                        if(l.toString().equals(layoutEnumStr)){
                            layoutEnum = l;
                            break;
                        }
                    }

                    for (GroupEnum g : GroupEnum.values()) {
                        if(g.getGroupName().equals(groupEnumStr)){
                            groupEnum = g;
                            break;
                        }
                    }

                    if(groupEnum == GroupEnum.DEMO)
                        showDemo();
                    else
                        callService(layoutEnum, groupEnum);
                }
            }
        });
    }

    private void setGroupSpinner() {
        List<GroupEnum> groupEnumList = Arrays.asList(GroupEnum.values());
        ArrayList<String> groupEnumStrList = new ArrayList<>();

        for (GroupEnum groupEnum : groupEnumList)
            groupEnumStrList.add(groupEnum.getGroupName());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getContext(),
                R.layout.spinner_text_template,
                groupEnumStrList);

        adapter.setDropDownViewResource(R.layout.spinner_dropdown_text_template);

        groupTypeSpinner.setAdapter(adapter);
    }

    private void setLayoutSpinner() {
        List<LayoutEnum> layoutEnumList = Arrays.asList(LayoutEnum.values());
        ArrayList<String> layoutEnumStrList = new ArrayList<>();

        for (LayoutEnum layoutEnum : layoutEnumList)
            layoutEnumStrList.add(layoutEnum.toString());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getContext(),
                R.layout.spinner_text_template,
                layoutEnumStrList);

        adapter.setDropDownViewResource(R.layout.spinner_dropdown_text_template);
        layoutTypeSpinner.setAdapter(adapter);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i(TAG, "onDestroy()");
    }

    private void setServerWorking(boolean flag){
        if(flag)
            loadingText.setVisibility(View.VISIBLE);
        else
            loadingText.setVisibility(View.GONE);

        isServerWorkingNow = flag;
    }
}
