package pl.wat.biblegraph.app.ui.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import pl.wat.biblegraph.app.ui.fragment.GraphInfoFragment;
import pl.wat.biblegraph.app.ui.fragment.LegendInfoFragment;
import pl.wat.biblegraph.app.ui.fragment.OptionsFragment;
import pl.wat.biblegraph.app.ui.fragment.VertexInfoFragment;

/**
 * Created by Lenovo on 2017-10-17.
 */

public class SectionPagerAdapter extends FragmentPagerAdapter {

    public SectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new VertexInfoFragment();
            case 1:
                return new GraphInfoFragment();
            case 2:
                return new LegendInfoFragment();
            case 3:
            default:
                return new OptionsFragment();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Wierzchołek";
            case 1:
                return "Graf";
            case 2:
                return "Legenda";
            case 3:
            default:
                return "Opcje";
        }
    }
}
