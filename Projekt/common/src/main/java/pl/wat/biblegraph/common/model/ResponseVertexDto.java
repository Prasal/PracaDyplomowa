package pl.wat.biblegraph.common.model;

import java.util.Map;

import lombok.Data;

/**
 * Created by Lenovo on 2017-07-25.
 */

@Data
public class ResponseVertexDto {
    private PersonRecord personRecord;
    private Point point;
    private Map<String, Number> measuresMap;
}
