package pl.wat.biblegraph.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Lenovo on 2017-07-24.
 */

@Data
@AllArgsConstructor
public class Point {
    private double x;
    private double y;
}
