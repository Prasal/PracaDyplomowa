package pl.wat.biblegraph.common.enums;

/**
 * Created by Lenovo on 2017-08-11.
 */

public enum GroupEnum {
    GENEALOGY_OF_JESUS("Genealogy of Jesus"),
    TRIBE_OF_LEVI("Tribe of Levi"),
    TRIBE_OF_BENJAMIN("Tribe of Benjamin"),
    TRIBE_OF_JUDAH("Tribe of Judah"),
    TRIBE_OF_REUBEN("Tribe of Reuben"),
    TRIBE_OF_JOSEPH("Tribe of Joseph"),
    TRIBE_OF_ASHER("Tribe of Asher"),
    TRIBE_OF_GAD("Tribe of Gad"),
    TRIBE_OF_ISSACHAR("Tribe of Issachar"),
    TRIBE_OF_SIMEON("Tribe of Simeon"),
    TRIBE_OF_ZEBULUN("Tribe of Zebulun"),
    TRIBE_OF_NAPHTALI("Tribe of Naphtali"),
    TRIBE_OF_DAN("Tribe of Dan"),
    DEMO("Demo");

    private String groupName;

    GroupEnum (String groupName){
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }
}
