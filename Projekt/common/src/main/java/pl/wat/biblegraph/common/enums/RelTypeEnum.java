package pl.wat.biblegraph.common.enums;

/**
 * Created by Lenovo on 2017-06-29.
 */

public enum RelTypeEnum {
    PARENT_CHILD("parentChild"), SPOUSE("spouse"), SIBLING("sibling");

    private String value;

    RelTypeEnum(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
