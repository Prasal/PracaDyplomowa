package pl.wat.biblegraph.common.model;

import lombok.Getter;
import lombok.Setter;
import pl.wat.biblegraph.common.enums.RelTypeEnum;

/**
 * Created by Lenovo on 2017-06-27.
 */

@Getter
@Setter
public class RelationKey {
  private int primary;
  private int relatedTo;
  private RelTypeEnum relTypeByValue;
}
