package pl.wat.biblegraph.common.shared;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lombok.Setter;
import pl.wat.biblegraph.common.model.PersonRecord;
import pl.wat.biblegraph.common.model.RelationKey;

/**
 * Created by Lenovo on 2017-06-27.
 */

@Setter
public class CommonData {
    protected Map<Integer, PersonRecord> personRecordMap;
    protected Set<RelationKey> relationSet;
    protected Map<String, Set<PersonRecord>> groupMap;

    public CommonData() {
        personRecordMap = new HashMap<>();
        relationSet = new HashSet<>();
        groupMap = new HashMap<>();
    }

    public PersonRecord getPersonRecord(int i){
        return personRecordMap.get(i);
    }

    public Set<PersonRecord> getPersonsFromGroup(String groupName){
        return groupMap.get(groupName);
    }

    public Set<RelationKey> getRelationSet(){
        return relationSet;
    }
}
