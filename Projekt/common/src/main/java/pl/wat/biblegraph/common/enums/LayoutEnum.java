package pl.wat.biblegraph.common.enums;

/**
 * Created by Lenovo on 2017-08-11.
 */

public enum LayoutEnum {
    ISOMlayout, FRlayout, CircleLayout, KKlayout, SpringLayout;
}
