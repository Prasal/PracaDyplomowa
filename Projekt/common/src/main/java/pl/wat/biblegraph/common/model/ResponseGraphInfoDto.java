package pl.wat.biblegraph.common.model;

import java.util.Map;

import lombok.Data;

/**
 * Created by Lenovo on 2017-07-25.
 */

@Data
public class ResponseGraphInfoDto {
    private Map<String, Number> variousMeasuresForGraph;
    private Map<String, MaxCentralityDto> maxCentralitiesForGraphMap;
}
