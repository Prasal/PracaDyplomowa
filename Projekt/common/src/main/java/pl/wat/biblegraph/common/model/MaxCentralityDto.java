package pl.wat.biblegraph.common.model;

import lombok.Getter;

/**
 * Created by Lenovo on 2017-10-15.
 */

@Getter
public class MaxCentralityDto {
    private int id;
    private Number value;

    public MaxCentralityDto(int id, Number value) {
        this.id = id;
        this.value = value;
    }
}
