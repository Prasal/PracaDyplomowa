package pl.wat.biblegraph.common.shared;

import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import pl.wat.biblegraph.common.model.PersonRecord;
import pl.wat.biblegraph.common.model.RelationKey;

/**
 * Created by Lenovo on 2017-11-28.
 */

public class LoaderImpl {
    private CommonData commonData;
    private Map<Integer, PersonRecord> personRecordMap;
    private Set<RelationKey> relationSet;
    private Map<String, Set<PersonRecord>> groupMap;

    public LoaderImpl(CommonData commonData) {
        this.commonData = commonData;
        personRecordMap = new HashMap<>();
        relationSet = new HashSet<>();
        groupMap = new HashMap<>();
    }

    public void init(InputStreamReader isr1, InputStreamReader isr2, InputStreamReader isr3) {
        loadPeople(isr1);
        loadRelations(isr2);
        loadGroups(isr3);

        commonData.setPersonRecordMap(personRecordMap);
        commonData.setRelationSet(relationSet);
        commonData.setGroupMap(groupMap);
    }

    private void loadGroups(InputStreamReader isr){
        LoaderAbstract groupLoader = new LoaderAbstract() {
            @Override
            public void actionForEveryLine(String line) {
                String[] record = line.split(",");
                Integer primary = Integer.valueOf(record[0]);
                String groupName = record[1];

                if(!groupMap.containsKey(groupName))
                    groupMap.put(groupName, new HashSet<PersonRecord>());

                groupMap.get(groupName).add(personRecordMap.get(primary));
            }
        };

        groupLoader.load(isr);
    }

    private void loadRelations(InputStreamReader isr){
        LoaderAbstract relationLoader = new LoaderAbstract() {
            @Override
            public void actionForEveryLine(String line) {
                String[] record = line.split(",");

                Integer primary = Integer.valueOf(record[0]);
                Integer relatedTo = Integer.valueOf(record[1]);

                RelationKey relationKey = new RelationKey();
                relationKey.setPrimary(primary);
                relationKey.setRelatedTo(relatedTo);
                relationKey.setRelTypeByValue(CommonTools.getRelTypeByValue(record[2]));

                relationSet.add(relationKey);
            }
        };

        relationLoader.load(isr);
    }

    private void loadPeople(InputStreamReader isr){
        LoaderAbstract peopleLoader = new LoaderAbstract() {
            @Override
            public void actionForEveryLine(String line) {
                String[] record = line.split(",");
                PersonRecord personRecord = new PersonRecord();

                Integer id = Integer.valueOf(record[0]);

                personRecord.setId(id);
                personRecord.setName(record[1]);
                personRecord.setSurname(record[2]);
                personRecord.setProperName(record[3].equals("1"));
                personRecord.setGender(record[4]);
                personRecord.setBirthYear(record[5]);
                personRecord.setDeathYear(record[6]);
                personRecord.setBirthPlace(record[7]);
                personRecord.setDeathPlace(record[8]);

                personRecordMap.put(id, personRecord);
            }
        };

        peopleLoader.load(isr);
    }
}