package pl.wat.biblegraph.common.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Lenovo on 2017-06-27.
 */

@Getter
@Setter
public class PersonRecord {
    private int id;
    private String name;
    private String surname;
    private boolean isProperName;
    private String gender;
    private String birthYear;
    private String deathYear;
    private String birthPlace;
    private String deathPlace;
}
