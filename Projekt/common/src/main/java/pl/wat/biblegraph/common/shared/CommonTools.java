package pl.wat.biblegraph.common.shared;


import pl.wat.biblegraph.common.enums.RelTypeEnum;

/**
 * Created by Lenovo on 2017-07-11.
 */

public class CommonTools {
    public static RelTypeEnum getRelTypeByValue(String value){
        RelTypeEnum[] relTypeEna = RelTypeEnum.values();

        for(RelTypeEnum relTypeEnum : relTypeEna){
            if(relTypeEnum.getValue().equals(value))
                return relTypeEnum;
        }

        return null;
    }
}
