package pl.wat.biblegraph.common.model;

import pl.wat.biblegraph.common.enums.RelTypeEnum;
import lombok.Data;

/**
 * Created by Lenovo on 2017-07-25.
 */

@Data
public class ResponseEdgeDto {
    private PersonRecord primary;
    private PersonRecord relatedTo;
    private RelTypeEnum relType;
}
