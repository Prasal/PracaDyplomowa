package pl.wat.biblegraph.common.shared;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Lenovo on 2017-06-29.
 */

public abstract class LoaderAbstract {
    public void load(InputStreamReader isr){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(isr);
            String line;
            boolean isFirstLine = true;

            while ((line = reader.readLine()) != null) {
                if(!isFirstLine){
                    actionForEveryLine(line);
                }
                else
                    isFirstLine = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public abstract void actionForEveryLine(String line);
}
