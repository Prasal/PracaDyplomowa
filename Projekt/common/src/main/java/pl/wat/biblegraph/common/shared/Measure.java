package pl.wat.biblegraph.common.shared;


import java.util.HashMap;
import java.util.Map;

import edu.uci.ics.jung.algorithms.scoring.BetweennessCentrality;
import edu.uci.ics.jung.algorithms.scoring.ClosenessCentrality;
import edu.uci.ics.jung.algorithms.scoring.DegreeScorer;
import edu.uci.ics.jung.algorithms.scoring.EigenvectorCentrality;
import edu.uci.ics.jung.algorithms.shortestpath.DistanceStatistics;
import edu.uci.ics.jung.graph.Graph;
import pl.wat.biblegraph.common.model.MaxCentralityDto;
import pl.wat.biblegraph.common.model.PersonRecord;

/**
 * Created by Lenovo on 2017-07-20.
 */

public class Measure {
    public Map<Integer, Map<String, Number>> measureCentrality(Graph<PersonRecord, String> g){
        Map<Integer, Map<String, Number>> measuresMap = new HashMap<>();

        ClosenessCentrality<PersonRecord, String> closenessCentrality = new ClosenessCentrality<>(g);
        BetweennessCentrality<PersonRecord, String> betweennessCentrality = new BetweennessCentrality<>(g);
        DegreeScorer<PersonRecord> degreeScorer = new DegreeScorer<>(g);

        EigenvectorCentrality<PersonRecord, String> eigenvectorCentrality = new EigenvectorCentrality<>(g);
        eigenvectorCentrality.acceptDisconnectedGraph(true);
        eigenvectorCentrality.evaluate();

        int denominator;

        if(g.getVertexCount() > 2)
            denominator = (g.getVertexCount() - 1) * (g.getVertexCount() - 2) / 2;
        else
            denominator = 1;

        for(PersonRecord personRecord : g.getVertices()){
            Double eigenvectorCentralityVertexScore = eigenvectorCentrality.getVertexScore(personRecord);
            Double closenessCentralityVertexScore = closenessCentrality.getVertexScore(personRecord);
            Double betweennessCentralityVertexScore = betweennessCentrality.getVertexScore(personRecord) / denominator;
            Double degreeScorerVertexScore = (double)degreeScorer.getVertexScore(personRecord) / (g.getVertexCount()-1);

            Map<String, Number> measuresInnerMap = new HashMap<>();
            measuresInnerMap.put("eigen", eigenvectorCentralityVertexScore);
            measuresInnerMap.put("closeness", closenessCentralityVertexScore);
            measuresInnerMap.put("betweenness", betweennessCentralityVertexScore);
            measuresInnerMap.put("degree", degreeScorerVertexScore);

            measuresMap.put(personRecord.getId(), measuresInnerMap);
        }

        return measuresMap;
    }

    public Map<String, Number> measureDiameter(Graph<PersonRecord, String> g){
        Map<String, Number> measuresMap = new HashMap<>();

        double diameter = DistanceStatistics.diameter(g);

        if(diameter == Double.POSITIVE_INFINITY)
            diameter = -1;

        measuresMap.put("diameter", diameter);
        return measuresMap;
    }

    public Map<String, MaxCentralityDto> measureMax(Map<Integer, Map<String, Number>> measureMap){
        Map<String, MaxCentralityDto> measuresMap = new HashMap<>();

        Number maxEigen = -1;
        Number maxCloseness = -1;
        Number maxBetweenness = -1;
        Number maxDegree = -1;

        int idForMaxEigen = -1;
        int idForMaxCloseness = -1;
        int idForMaxBetweenness = -1;
        int idForMaxDegree = -1;

        for (Integer integer : measureMap.keySet()) {
            Map<String, Number> centralitiesMap = measureMap.get(integer);

            Number eigen = centralitiesMap.get("eigen");
            Number closeness = centralitiesMap.get("closeness");
            Number betweeness = centralitiesMap.get("betweenness");
            Number degree = centralitiesMap.get("degree");

            if(eigen.doubleValue() > maxEigen.doubleValue()){
                maxEigen = eigen;
                idForMaxEigen = integer;
            }

            if(closeness.doubleValue() > maxCloseness.doubleValue()){
                maxCloseness = closeness;
                idForMaxCloseness = integer;
            }

            if(betweeness.doubleValue() > maxBetweenness.doubleValue()){
                maxBetweenness = betweeness;
                idForMaxBetweenness = integer;
            }

            if(degree.doubleValue() > maxDegree.doubleValue()){
                maxDegree = degree;
                idForMaxDegree = integer;
            }
        }

        measuresMap.put("maxEigen", new MaxCentralityDto(idForMaxEigen, maxEigen));
        measuresMap.put("maxCloseness", new MaxCentralityDto(idForMaxCloseness, maxCloseness));
        measuresMap.put("maxBetweenness", new MaxCentralityDto(idForMaxBetweenness, maxBetweenness));
        measuresMap.put("maxDegree", new MaxCentralityDto(idForMaxDegree, maxDegree));

        return measuresMap;
    }
}
