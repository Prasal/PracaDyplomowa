package pl.wat.biblegraph.common.model;

import java.util.List;

import lombok.Data;

/**
 * Created by Lenovo on 2017-07-11.
 */

@Data
public class ResponseArray {
    private List<ResponseVertexDto> responseVertexDtos;
    private List<ResponseEdgeDto> responseEdgeDtos;
    private ResponseGraphInfoDto responseGraphInfoDto;
}
